# Mobile Network Latency Measurement Tools
latency_meas is an application that help to measure the latency of your mobile network, such as, 5G network and Wifi network.
## Installation
Clone this repository and import into Android Studio
```
git@gitlab.com:meng0219/latency_meas.git
```
## Usage
There are three main tools in this app. The followings are their decription and use way.
### Ping
This tool uses ICMP packet to verify if a particular destination exists and can accept requests in network administration.
1. Press "PING" button to Go to Ping page.
2. Fill the following items in order.
    1. "Destination" - the Ip or domain name of destination.
    2. "Total Packets" - number of ICMP packets will be sent.
    3. "Interval" - the interval slot between two consecutive ICMP packets.
    4. "Payload" - the size of ICMP packet.
3. Press "START" button to start measurement.
4. The testing result will be displayed in the scrolling view below.
### Trace
This tool is referenced from https://github.com/yanbober/android-tracepath.
It display all latency between the node in the path to a particular destination and the device.
1. Press "TRACE" button to Go to Trace page.
2. Fill the "Destination" (the Ip or domain name of destination)
3. Press "START" button to start measurement.
4. The testing result will be displayed in the scrolling view below.
### TWAMP
This tool uses TWAMP to measure network performance between a particular destination and the device.
#### What is TWAMP
The Two-Way Active Measurement Protocol (TWAMP) is an open protocol for measuring network performance between any two devices in a network that supports the protocols in the TWAMP framework.
#### How to calculate
##### Delay
The delay is calculated based on time stamps. The TWAMP-Test packet sent by the controller carries the sending time t1, the TWAMP-Test packet sent by the responder carries the receiving time t1' and reply time t2', and the TWAMP-Test packet received by the controller carries the receiving time t2. The delay is calculated based on the four time stamps.
```
Delay1 = t2 - t1 - (t2' - t1')
```
##### Jitter
The jitter is calculated based on the absolute delays in neighboring measurement intervals.
Based on the preceding delay formula, the delay in the neighboring interval is Delay2 = t4 - t3 - (t4' - t3').
```
Jitter = | Delay2 - Delay1 |
```
1. Press "TWAMP" button to Go to TWAMP page.
2. Fill the following items in order.
    1. "Destination" - the Ip or domain name of destination.
    2. "Port" - the TWAMP control port of destination.
    2. "Total Packets" - number of TWAMP packets will be sent.
    3. "Interval" - the interval slot between two consecutive TWAMP packets.
    4. "Payload" - the size of TWAMP packet.
3. Make sure the TWAMP server is already enabled on the destination device.
4. Press "START" button to start measurement.
5. The testing result will be displayed in the scrolling view below.


